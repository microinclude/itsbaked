import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from 'src/app/models/product.model';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss'],
})
export class ItemCardComponent {
  @Input()
  product: Product;

  @Output()
  actionViewProduct: EventEmitter<Product> = new EventEmitter();

  constructor() {}

  viewProduct() {
    this.actionViewProduct.emit(this.product);
  }
}
