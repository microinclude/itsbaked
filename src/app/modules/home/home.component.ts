import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor() {}

  productList: Product[] = [
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      title: 'Cheesecake',
      price: '200.00',
      discountList: ['50% discount'],
      currency: 'PHP',
      description: 'Box of 6, random flavors',
      image:
        'https://images.pexels.com/photos/1546892/pexels-photo-1546892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
  ];

  ngOnInit(): void {
    console.log('Initialized');
  }
}
