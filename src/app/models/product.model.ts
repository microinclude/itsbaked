export interface Product {
  title: string;
  description: string;
  currency: string;
  price: string;
  discountList: string[];
  image: string;
}
