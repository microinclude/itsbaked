import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
{
  path: '',
  loadChildren: () => import("./modules/home/home.module").then(m => m.HomeModule)
},
{
  path: '404',
  loadChildren: () => import("./modules/error/error.module").then(m => m.ErrorModule)
},
{
  path: 'order',
  loadChildren: () => import("./modules/order/order.module").then(m => m.OrderModule)
},
{
  path: "**",
  redirectTo: "/404"
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
